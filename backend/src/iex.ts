import axios, { AxiosInstance } from 'axios';

axios.defaults.adapter = require('axios/lib/adapters/http');

export interface IIexBroker {
  token: string;
  http: AxiosInstance;
  getFinancials: Function;
  getDelayedQuote: Function;
}

const IexBroker = class {
  token: string;
  http: AxiosInstance;
  constructor(token: string, sandbox: boolean = true) {
    this.token = token;

    if (sandbox) {
      this.http = axios.create({
        baseURL: 'https://sandbox.iexapis.com/stable',
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } else {
      this.http = axios.create({
        baseURL: 'https://cloud.iexapis.com/stable',
        headers: {
          'Content-Type': 'application/json',
        },
      });
    }
  }

  async getFinancials(
    ticker: string,
    period: string = 'annual',
    range: string = 'latest'
  ) {
    try {
      const res = await this.http.get(
        `/time-series/fundamentals/${ticker.toLowerCase()}/${period.toLowerCase()}`,
        {
          params: {
            token: this.token,
            range: range,
          },
        }
      );

      if (res.status === 200) {
        if (res.data.length !== 1) {
          return res.data;
        }
        return res.data[0];
      }
    } catch (err: any) {
      if (err.response) {
        // Request made but the server responded with an error
        await Promise.reject(
          new Error(`${err.response.status} - ${err.response.data}`)
        );
      } else if (err.request) {
        // Request made but no response is received
        new Error(err);
      } else {
        new Error(err);
      }
    }
  }

  async getDelayedQuote(ticker: string) {
    try {
      const res = await this.http.get(
        `/stock/${ticker.toLowerCase()}/delayed-quote`,
        {
          params: {
            token: this.token,
          },
        }
      );

      if (res.status === 200) {
        return res.data;
      }
    } catch (err: any) {
      if (err.response) {
        // Request made but the server responded with an error
        await Promise.reject(
          new Error(`${err.response.status} - ${err.response.data}`)
        );
      } else if (err.request) {
        // Request made but no response is received
        new Error(err);
      } else {
        new Error(err);
      }
    }
  }
};

//module.exports = IexBroker;
export default IexBroker;
