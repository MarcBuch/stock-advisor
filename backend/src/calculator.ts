export const calculateROCE = (
  ebit: number,
  totalAssets: number,
  currentLiabilities: number
) => {
  return ebit / (totalAssets - currentLiabilities);
};
