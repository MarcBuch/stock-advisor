import { calculateROCE } from '../calculator';

describe('calculateROCE', () => {
  test('returns not null', () => {
    const data = calculateROCE(118329, 336309, 129873);

    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect(data).not.toBeNaN();
  });

  test('returns the correct result', () => {
    expect(calculateROCE(118329, 336309, 129873)).toEqual(0.573199441957798);
  });
});
