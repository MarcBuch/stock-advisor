import IexBroker from '../iex';
import nock from 'nock';

const iexBroker = new IexBroker('123');

function nockSetup() {
  afterAll(nock.cleanAll);
  afterEach(nock.restore);
  beforeEach(() => {
    if (!nock.isActive()) {
      nock.activate();
    }
  });
}

const baseUrl = 'https://sandbox.iexapis.com/stable';
let stock = 'aapl';
let errReply = '404 - Unknown symbol';

describe('getFinancials', () => {
  nockSetup();

  const url = `/time-series/fundamentals/${stock}/annual?token=123&range=latest`;

  test('returns not null', async () => {
    const scope = nock(baseUrl)
      .get(url)
      .reply(200, [
        {
          accountsPayable: 123,
          accountsPayableTurnover: 123,
          accountsReceivable: 123,
        },
      ]);

    const data = await iexBroker.getFinancials(stock);

    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect(data).not.toBeNaN();
    scope.done();
  });

  test('handles error response', async () => {
    const scope = nock(baseUrl).get(url).reply(404, errReply);

    await expect(iexBroker.getFinancials(stock)).rejects.toThrow(errReply);
    scope.done();
  });
});

describe('getDelayedQuote', () => {
  nockSetup();

  const url = `/stock/${stock}/delayed-quote?token=123`;

  test('returns not null', async () => {
    const scope = nock(baseUrl).get(url).reply(200, {
      symbol: 'AAPL',
      delayedPrice: 143.08,
      delayedSize: 200,
    });

    const data = await iexBroker.getDelayedQuote(stock);

    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect(data).not.toBeNaN();
    scope.done();
  });

  test('handles error response', async () => {
    const scope = nock(baseUrl).get(url).reply(404, errReply);

    await expect(iexBroker.getDelayedQuote(stock)).rejects.toThrow(errReply);
    scope.done();
  });
});
