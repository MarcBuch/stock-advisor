import IexBroker, { IIexBroker } from './iex';
import { calculateROCE } from './calculator';

const Facade = class {
  iexBroker: IIexBroker;
  constructor(process: NodeJS.Process) {
    const token = String(process.env.IEX_TOKEN);
    this.iexBroker = new IexBroker(token);
  }

  async getROCE(
    ticker: string,
    period: string = 'annual',
    range: string = 'latest'
  ) {
    const financials = await this.iexBroker.getFinancials(ticker.toLowerCase());

    return calculateROCE(
      financials['ebitReported'],
      financials['assetsUnadjusted'],
      financials['liabilitiesCurrent']
    );
  }
};

export default Facade;
