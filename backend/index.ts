import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

import Facade from './src/facade';

dotenv.config();

const app: Express = express();

const port = process.env.PORT;
const facade = new Facade(process);

app.get('/', async (req: Request, res: Response) => {
  if (Object.keys(req.query).length === 0) {
    return res.status(400).send('Please provide a query');
  }

  if (req.query.stock) {
    const roce = await facade.getROCE(String(req.query.stock));
    return res.status(200).send(String(roce));
  }
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});
